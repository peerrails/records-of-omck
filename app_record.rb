require 'pg'
require 'sequel'
require 'cocaine'

#sudo docker run --name povtori -d -v /data/videos:/data/videos --link postgres-sama:pg -t PG_ENV_POSTGRES_DATABASE= -e PG_ENV_POSTGRES_PASSWORD= -e PG_ENV_POSTGRES_USER= -e RTMP_URL= records

module App
  class Record

    def self.connect (host, dba, us, pass)
      @db = Sequel.postgres(dba, :host=>host, :user=>us, :password=>pass)
      p "Connected to Database"
    end

    def self.disconnect
      @db.disconnect
      p "Disconnected"
    end

    def self.set_status id, status
      @db[:playlists].where(id: id).update(status: status)
    end

    def self.push

      host = ENV["PG_PORT_5432_TCP_ADDR"] || "172.17.42.1"
      dba = ENV["PG_ENV_POSTGRES_DATABASE"] || "omckonrails-dev"
      us = ENV["PG_ENV_POSTGRES_USER"] || "dev"
      pass = ENV["PG_ENV_POSTGRES_PASSWORD"] || "dev"
      rtmp_url = ENV["RTMP_URL"] || "172.17.42.1"

      connect(host, dba, us, pass)

      pl_ids = @db[:playlists].select(:video_id).map { |e| e[:video_id] }
      pl_ids = [0] if pl_ids.empty?


      @db[:playlists].with_sql_delete("DELETE from playlists as p using videos as v where (p.video_id = v.id AND v.deleted = true) OR p.status = 5")

      @db[:playlists].with_sql_insert("INSERT INTO playlists
                                      (video_id, created_at, updated_at, path, status)
                                      SELECT id as video_id,
                                                   now() at time zone 'utc' as created_at,
                                                   now() at time zone 'utc' as updated_at,
                                                   path,
                                                   0 as status
                                      FROM videos as v
                                      WHERE deleted = false and not (v.id = ANY(array#{pl_ids}) );")

      play = @db[:playlists].select(:id, :path, :video_id, :status).where(status: 0).order(:id).last
      if !play.nil? && play[:status] != 5 && File.exist?(play[:path])
        p play[:path]
        line = Cocaine::CommandLine.new("avconv -re -i #{play[:path]} -f flv rtmp://#{rtmp_url}/records/omcktv")
        `touch start.job`
        begin
          set_status(play[:id], 2)
          line.run
          set_status(play[:id], 1)
        rescue Cocaine::ExitStatusError => e
          set_status(play[:id], 3)
        end
        `rm start.job`
      elsif !play.nil? && play[:status] == 5
        File.delete(play[:path])
      else
        @db[:playlists].where("status != 3 OR status != 5").update(status: 0)
      end
      disconnect

    end
  end
end
