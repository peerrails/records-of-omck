FROM ruby:onbuild

RUN apt-get update && \
    apt-get install -y libav-tools \
    && rm -rf /var/lib/apt/lists/*
# Add omck-on-cron
RUN cd /usr/src/app && bundle install

# Run the command on container startup
# env -e PG_ENV_POSTGRES_PASSWORD=dev --volume /data/videos:/data/videos
CMD ruby /usr/src/app/main.rb >> /usr/src/app/log.txt
